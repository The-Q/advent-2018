module Utils

    open System
    open System.IO

    let readFile(path:String) =
        File.ReadAllText(path)

    let readLines path = 
        File.ReadAllLines path  
