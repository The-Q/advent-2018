// Learn more about F# at http://fsharp.org

open System
open Utils
open Day6

[<EntryPoint>]
let main argv =
    let input = readLines @"Inputs\day6.txt"
    let answer1 = task1 input
    let answer2 = task2 input
    printfn "First answer is: %d" answer1
    printfn "Second answer is: %d" answer2
    0 // return an integer exit code
