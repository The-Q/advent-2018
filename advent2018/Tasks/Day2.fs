module Day2
    open System
    open System.Collections.Generic

    module Seq =
        let frequences xs =
            xs
            |> Seq.groupBy id
            |> Seq.map (fun (k,v) -> (k, Seq.length v))
            |> dict

    let hasEntriesWithCount value (dict: IDictionary<'a,'b>) =
        dict.Values.Contains(value)

    let isDiff (x,y) =
        x <> y

    let countDifferent xs =
        Seq.filter isDiff xs
        |> Seq.length

    let collectString xs =
        Seq.filter (isDiff >> not) xs
        |> Seq.map fst
        |> Seq.toArray
        |> String

    let foldingFunc acc elem =
        if countDifferent elem = 1
        then (collectString elem) :: acc
        else acc

    let finder h r =
        Seq.allPairs [h] r
        |> Seq.map (fun (x,y) -> Seq.zip x y)
        |> Seq.fold foldingFunc []

    let task1 input =
        let freqs = input |> Seq.map Seq.frequences
        let twoCountFun = freqs |> Seq.filter (hasEntriesWithCount 2) |> Seq.length
        let threeCountFun = freqs |> Seq.filter (hasEntriesWithCount 3) |> Seq.length
        twoCountFun * threeCountFun

    let task2 input =
        let rec inner xs acc =
           match xs with
            | h::r -> inner r (List.append acc (finder h r))
            | [] -> acc
        inner input []
