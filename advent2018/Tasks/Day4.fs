module Day4
    open System
    open System.Text.RegularExpressions
    open System.Collections.Generic
    open System.Linq
    open Day1of2017

    let actionRegex = Regex("^\[(\d+-\d+-\d+ \d+:\d+)\] (.*)$", RegexOptions.Compiled)
    let dutyRegex = Regex("^Guard #(\d+) begins shift$", RegexOptions.Compiled)

    type State =
    | Duty of int
    | Sleep
    | Wake

    type InputRecord = {
        Date: DateTime;
        State: State
    }
    type GuardStats = {
        TimeSlept: int;
        Minutes: seq<int>
    } with
        static member Empty = {TimeSlept =  0; Minutes= Seq.empty}

    type FoldState = {
        Dict: IDictionary<int, GuardStats>;
        CurrentId: int;
        StartSleep: DateTime
    }

    let getState (s:string) =
        let parseDuty () =
            let groups = (dutyRegex.Match s).Groups
            ((groups.Item 1).Value) |> int
        if s.StartsWith("G")
        then Duty(parseDuty ())
        else if s.StartsWith("f")
        then Sleep
        else Wake

    let parseLine (s:string) =
        let lineGroups = (actionRegex.Match s).Groups
        let date = (lineGroups.Item 1).Value |>DateTime.Parse
        let state = (lineGroups.Item 2).Value |> getState
        {
            Date = date;
            State = state;
        }

    let prepareInput lines =
        lines
        |> Seq.map parseLine
        |> Seq.sortBy (fun x -> x.Date)

    let fixSleep acc (date:DateTime) =
        let dict = acc.Dict
        let minutes = seq { acc.StartSleep.Minute .. date.Minute-1 }
        let duration = Seq.length minutes
        let result_so_far = match dict.TryGetValue acc.CurrentId with
                                |true, value -> value
                                |false, _ -> GuardStats.Empty
        dict.[acc.CurrentId] <- {
            result_so_far with
                TimeSlept = result_so_far.TimeSlept + duration;
                Minutes = Seq.append result_so_far.Minutes minutes;
        }
        acc

    let proccessState (acc:FoldState) (elem:InputRecord) =
        match elem.State with
            |Duty id -> {acc with CurrentId = id}
            |Sleep -> {acc with StartSleep = elem.Date}
            |Wake -> fixSleep acc elem.Date

    let calcStats input =
        let input = prepareInput input
        let startState = {
            Dict= new Dictionary<int, GuardStats>();
            CurrentId = 0;
            StartSleep = DateTime.Now
        }
        Seq.fold proccessState startState input

    let task1 input =
        let finalState = calcStats input
        let maxSleptRecord = (finalState.Dict |>Seq.maxBy (fun x -> x.Value.TimeSlept))
        let mostComonnMinute = Seq.countBy id maxSleptRecord.Value.Minutes |> Seq.maxBy snd |> fst
        mostComonnMinute * maxSleptRecord.Key

    let task2 input =
        let finalState = calcStats input
        let mostFrequentMinute =
            finalState.Dict
            |> Seq.map (fun x -> (x.Key, Seq.countBy id x.Value.Minutes |> Seq.maxBy snd))
            |> Seq.maxBy (fun x -> snd x |> snd)
        fst mostFrequentMinute * (snd mostFrequentMinute |> fst)





