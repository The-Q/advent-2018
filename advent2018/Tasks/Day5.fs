module Day5
    open System
    open System.Collections.Generic

    let shouldAnigilate (c1:char) (c2:char) =
        abs(int(c1) - int(c2)) = 32

    let processPolymer acc c =
        match acc with
        | h::t -> if shouldAnigilate h c then t else c::acc
        | [] -> [c]

    let proccessAndGetLength input =
         Seq.fold processPolymer [] input
        |> List.length

    let removeUnit chain (unit:char) =
        chain
        |> Seq.filter (fun x ->Char.ToUpper x <> Char.ToUpper unit)

    let task1 =
       proccessAndGetLength

    let iterFunc chain (d:IDictionary<char,int>) unit =
        let len = removeUnit chain unit
                  |> proccessAndGetLength
        d.Add(unit, len)

    let task2 input =
        let units = Seq.distinctBy Char.ToUpper input
        let resultDict = new Dictionary<char,int>()
        let f = iterFunc input resultDict
        units |> Seq.iter f
        (resultDict |> Seq.minBy (fun kv -> kv.Value)).Value

