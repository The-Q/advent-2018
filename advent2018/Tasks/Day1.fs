module Day1
    module private Seq = let rec cycle xs = seq {yield! xs; yield! cycle xs}
    open System

    let task1 input =
        input |> Seq.sumBy Int64.Parse

    let task2 input =
        input 
        |> Seq.map Int64.Parse
        |> Seq.cycle
        |> Seq.scan (fun (f, seen) d ->  f + d, seen |> Set.add f) (0L, Set.empty)
        |> Seq.find (fun (f, seen) -> seen |> Set.contains f)
        |> fst