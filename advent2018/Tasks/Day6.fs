module Day6
    open System

    type Point ={X: int; Y: int; Owner:int}

    let pointFromArray idx (arr:int[])   =
        {X = arr.[0]; Y = arr.[1]; Owner = idx }

    let distance p1 p2 =
        abs(p1.X - p2.X) + abs(p1.Y - p2.Y)

    let parseLine idx (l:String) =
        l.Split([|','|], StringSplitOptions.RemoveEmptyEntries)
        |> Array.map (fun l -> l.Trim() |> int)
        |> pointFromArray idx

    let genPointsInArea topLeftPoint bottomRightPoint =
        seq
            { for y in topLeftPoint.Y .. bottomRightPoint.Y do
                for x in topLeftPoint.X .. bottomRightPoint.X do
                    yield {X = x; Y = y; Owner = Int32.MinValue}
            }

    let calcPointOwner points currentPoint =
        let distances =  points
                        |> Seq.map (fun p -> p.Owner,distance currentPoint p)

        let minVal = distances
                     |> Seq.minBy snd
        let count = Seq.filter (fun (_,y) -> y = snd minVal)  distances |> Seq.length
        if count = 1
        then {currentPoint with Owner = fst minVal}
        else currentPoint

    let calcPointDistance points currentPoint =
        let distances =  points
                        |> Seq.map (fun p -> distance currentPoint p)

        let totalDistance = Seq.sum distances
        if totalDistance < 10000
        then {currentPoint with Owner = Int32.MaxValue}
        else currentPoint

    let isOnBorder topLeftPoint bottomRightPoint point =
        point.Y = topLeftPoint.Y
        || point.Y = bottomRightPoint.Y
        || point.X = topLeftPoint.X
        || point.X = bottomRightPoint.X

    let ownersOfBorderPoints topLeftPoint bottomRightPoint  points =
        points
        |> Seq.filter (isOnBorder topLeftPoint bottomRightPoint)
        |> Seq.distinctBy (fun x -> x.Owner)
        |> Seq.map (fun x -> x.Owner)

    let task1 input =
        let startPoints = input |> Seq.mapi parseLine
        let minx = (startPoints |> Seq.minBy (fun x -> x.X)).X
        let miny = (startPoints |> Seq.minBy (fun x -> x.Y)).Y
        let maxx = (startPoints |> Seq.maxBy (fun x -> x.X)).X
        let maxy = (startPoints |> Seq.maxBy (fun x -> x.Y)).Y
        let topLeft = {X = minx - 1; Y = miny - 1; Owner = Int32.MinValue}
        let bottomRigth = {X = maxx + 1; Y = maxy; Owner = Int32.MinValue}
        let points = genPointsInArea topLeft bottomRigth
        let pointsWithOwners = Seq.map (calcPointOwner startPoints) points
        let borderOwners = ownersOfBorderPoints topLeft bottomRigth pointsWithOwners |> set
        pointsWithOwners
        |> Seq.filter (fun x -> borderOwners.Contains(x.Owner) |> not)
        |> Seq.countBy (fun x -> x.Owner)
        |> Seq.maxBy snd
        |> snd

    let task2 input =
        let startPoints = input |> Seq.mapi parseLine
        let minx = (startPoints |> Seq.minBy (fun x -> x.X)).X
        let miny = (startPoints |> Seq.minBy (fun x -> x.Y)).Y
        let maxx = (startPoints |> Seq.maxBy (fun x -> x.X)).X
        let maxy = (startPoints |> Seq.maxBy (fun x -> x.Y)).Y
        let topLeft = {X = minx - 1; Y = miny - 1; Owner = Int32.MinValue}
        let bottomRigth = {X = maxx + 1; Y = maxy; Owner = Int32.MinValue}
        let points = genPointsInArea topLeft bottomRigth
        Seq.map (calcPointDistance startPoints) points
        |> Seq.filter (fun x -> x.Owner = Int32.MaxValue)
        |> Seq.length