module Day1of2017
    module private Seq = let rec cycle xs = seq {yield! xs; yield! cycle xs}
    open System

    let private charToLong (c:Char) =
        match c with
        |'1' -> 1L
        |'2' -> 2L
        |'3' -> 3L
        |'4' -> 4L
        |'5' -> 5L
        |'6' -> 6L
        |'7' -> 7L
        |'8' -> 8L
        |'9' -> 9L
        |'0' -> 0L
        |_ -> raise (new Exception())

    let private createShifted input offset =
        input
        |> Seq.cycle
        |> Seq.skip offset
        |> Seq.map charToLong

    let task1 (input:String) =
        let shifted =
            createShifted input 1
        input
        |> Seq.map charToLong
        |> Seq.zip shifted
        |> Seq.filter (fun (x,y) -> x = y)
        |> Seq.fold (fun acc (x,_) -> acc + x) 0L

    let task2(input:String) =
        let offset = input.Length / 2
        let shifted =
            createShifted input offset
        input
        |> Seq.map charToLong
        |> Seq.zip shifted
        |> Seq.filter (fun (x,y) -> x = y)
        |> Seq.fold (fun acc (x,_) -> acc + x) 0L

