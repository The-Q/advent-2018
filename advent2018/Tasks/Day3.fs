module Day3
    open System.Text.RegularExpressions
    open System.Collections.Generic
    open System.Collections.Generic

    type Cut = {
        id: string;
        top: int;
        left: int;
        width: int;
        heigth: int
    } with
        member this.Sqare = this.width * this.heigth

    let parseLine l =
        let g = (Regex.Match (l,"^#(\d+) @ (\d+),(\d+): (\d+)x(\d+)$")).Groups
        {
            id     = (g.Item 1).Value
            left   = (g.Item 2).Value |> int;
            top    = (g.Item 3).Value |> int;
            width  = (g.Item 4).Value |> int;
            heigth = (g.Item 5).Value |> int;
        }


    let gen cut =
        seq{
            for y in  cut.top .. cut.top + cut.heigth - 1 do
                for x in cut.left .. cut.left + cut.width - 1 do
                   yield (x,y)
        }

    let runner1 (d: IDictionary<int*int, int>) c =
        for point in gen c do
            if d.ContainsKey point
            then
                let v = d.[point]
                d.[point] <-  v+1
            else d.Add(point, 1)

    let runner2 (d: IDictionary<int*int, string>) c =
        for point in gen c do
            if d.ContainsKey point
            then
                let v = d.[point]
                d.[point] <-  "X"
            else d.Add(point, c.id)

    let filterFun (d: IDictionary<string, int>) (c: Cut) =
        if d.ContainsKey c.id
        then d.[c.id] = c.Sqare
        else false

    let task1 input =
        let cuts = Seq.map parseLine input
        let d = new Dictionary<int*int, int>()
        Seq.iter (runner1 d)  cuts
        d.Values
        |> Seq.filter (fun x -> x > 1)
        |> Seq.length

    let task2 input =
        let cuts = Seq.map parseLine input
        let d = new Dictionary<int*int, string>()
        Seq.iter (runner2 d)  cuts
        let counts = d.Values |> Seq.countBy id |> dict
        (Seq.filter (filterFun counts) cuts |> Seq.exactlyOne).id
